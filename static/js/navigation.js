var leftNavOpen = document.querySelector('#about');
var leftNavClose = document.querySelector('#leftnavclose');
var windowOpen = false;

function toggleLeftNav() {
  document.querySelector("#aboutNav").classList.toggle('hidden');
};

leftNavOpen.onclick = toggleLeftNav;
leftNavClose.onclick = toggleLeftNav;

var rightNavOpen = document.querySelector('#add');
var rightNavClose = document.querySelector('#rightnavclose');

function toggleRightNav() {
  document.querySelector("#addNav").classList.toggle('hidden');
  windowOpen = !windowOpen;

};

rightNavOpen.onclick = toggleRightNav;
rightNavClose.onclick = toggleRightNav;