from django.db import migrations


def forwards(apps, schema_editor):
    try:
        SpatialRefSys = apps.get_model('gis', 'PostGISSpatialRefSys')
    except LookupError:
        # The old app isn't installed.
        return

    SpatialRefSys.objects.exclude(srid=4326).delete()

# Cannot go backwards without re-running the PostGIS SQL file


class Migration(migrations.Migration):

    dependencies = [
        ('pinprick', '0003_moment_approved_by'),
    ]

    operations = [
        migrations.RunPython(forwards, migrations.RunPython.noop),
    ]
