from django import forms
from nocaptcha_recaptcha.fields import NoReCaptchaField
from nocaptcha_recaptcha.widgets import InvisibleReCaptchaWidget

from .models import Moment, mkpoint


class SubmitMomentForm(forms.ModelForm):
    latitude = forms.FloatField()
    longitude = forms.FloatField()
    NoReCaptchaField(
        gtag_attrs={
            # Fill these in if we ever do Django-based form rendering
        },
        widget=InvisibleReCaptchaWidget
    )

    # Is there a better way to handle converting lat/lng into a point?

    def clean_latitude(self):
        if 'longitude' in self.cleaned_data:
            self.instance.location = mkpoint(self.cleaned_data['latitude'], self.cleaned_data['longitude'])

        return self.cleaned_data['latitude']

    def clean_longitude(self):
        if 'latitude' in self.cleaned_data:
            self.instance.location = mkpoint(self.cleaned_data['latitude'], self.cleaned_data['longitude'])

        return self.cleaned_data['longitude']

    class Meta:
        model = Moment
        fields = ['description']

    def clean_captcha(self):
        print("initial", self.initial)
        print("cleaned_data", self.cleaned_data)
        print("vars", vars(self))
