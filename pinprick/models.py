from django.contrib.gis.db import models
from model_utils.models import StatusModel, TimeStampedModel
from model_utils import Choices, FieldTracker
from django.contrib.gis.geos import Point
from django.dispatch import receiver
from django.db.models.signals import pre_save
from django.conf import settings


def mkpoint(lat, lng):
    return Point(lng, lat)


# https://django-model-utils.readthedocs.io/en/latest/models.html#statusmodel
class Moment(StatusModel, TimeStampedModel):
    STATUS = Choices('unchecked', 'auto_spam', 'auto_ham', 'manual_spam', 'manual_ham')
    location = models.PointField(geography=True)
    description = models.TextField()
    approved_by = models.ForeignKey(on_delete=models.PROTECT, to=settings.AUTH_USER_MODEL, null=True)

    @property
    def latitude(self):
        if self.location is not None:
            return self.location.y

    @property
    def longitude(self):
        if self.location is not None:
            return self.location.x

    tracker = FieldTracker()

    def __str__(self):
        return self.description

    @classmethod
    def from_latlong(cls, latitude, longitude, description, status=STATUS.unchecked):
        return cls(
            location=mkpoint(latitude, longitude),
            description=description,
            status=status,
        )

    def serialize(self):
        return {
            "latitude": self.latitude,
            "longitude": self.longitude,
            "description": self.description,
            "id": self.pk
        }

    # @classmethod
    # def deserialize(cls)


@receiver(pre_save, sender=Moment)
def autocheck(sender, **kwargs):
    if sender.status in (Moment.STATUS.unchecked, Moment.STATUS.auto_ham):
        # Check the length
        if len(sender.description) > 600:  # a little over two jumbo tweets
            sender.status = Moment.STATUS.auto_ham
