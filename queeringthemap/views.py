from django.views.generic.base import TemplateView
from django.conf import settings
from django.utils.decorators import method_decorator
from django.contrib.admin.views.decorators import staff_member_required
from django.core.paginator import Paginator
from django.shortcuts import render

from pinprick.models import Moment


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['recaptcha_site_key'] = settings.NORECAPTCHA_SITE_KEY
        context['google_maps_key'] = settings.GOOGLE_MAPS_KEY
        return context


class ModeratorView(TemplateView):

    """ Get: Display a list of map points that have been submitted for moderation
        Post: Approve or deny a map point that has been submitted for moderation
    """
    @method_decorator(staff_member_required)
    def get(self, request, **kwargs):

        POSTS_PER_PAGE = 10

        all_unmoderated_posts = Moment.objects.filter(status__exact='unchecked')
        paginator = Paginator(all_unmoderated_posts, POSTS_PER_PAGE)

        page = request.GET.get('page')
        unmoderated_posts = paginator.get_page(page)

        return render(request, 'moderator.html', {'unmoderated_posts': unmoderated_posts})

    @method_decorator(staff_member_required)
    def post(self, request, **kwargs):

        moderator_action = request.POST.get('moderator_action')
        ALLOWED_MODERATOR_ACTIONS = ('hide', 'show')

        context = {}

        if moderator_action in ALLOWED_MODERATOR_ACTIONS and request.POST.get('pin_post_id'):
            try:
                this_pin = Moment.objects.get(id=request.POST.get('pin_post_id'))
            except Moment.DoesNotExist:
                context['status'] = '[ERROR] Pin does not exist.'
            else:
                if moderator_action == 'hide':
                    this_pin.status = 'manual_spam'
                elif moderator_action == 'show':
                    this_pin.status = 'manual_ham'
                this_pin.save()
                context['status'] = 'Success'
        elif moderator_action not in ALLOWED_MODERATOR_ACTIONS:
            context['status'] = '[ERROR] {0} is not one of the ALLOWED_MODERATOR_ACTIONS ({1})'.format(
                moderator_action, ALLOWED_MODERATOR_ACTIONS
            )
        elif not request.POST.get('pin_post_id'):
            context['status'] = '[ERROR] pin_post_id not provided'

        return render(request, 'moderator_action.html', context)
