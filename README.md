# queeringthemap

## Description
_Queering the Map_ is a community-generated mapping project that geo-locates queer moments, memories and histories in both cyber and physical space. As queer life becomes increasingly less centered around specific neighborhoods and the buildings within them, notions of ‘queer spaces’ become more abstract and less tied to concrete geographical locations. The intent of the _Queering the Map_ project is to ‘queer’ as much space as possible, from park benches to parking garages, to mark moments of queerness wherever they occur.

There are no guidelines to what constitutes an act of queering space. If it counts to you, then it counts for _Queering the Map_. Anything from direct action activism to a conversation expressing preferred pronouns, from flirtatious glances to weekend long sex parties; all are part of the project of queering space. Queer history matters, and elders of the community are encouraged to add moments and places of historical significance to the map that enrich our collective memory.

Through mapping these ephemeral moments, _Queering the Map_ hopes to create an interlocking web of queerness in digital and physical space.

## Want to contribute? Let's get you set up first.

1. Make sure you have [pipenv](https://github.com/pypa/pipenv) installed.
1. Clone this repository and enter it: `git clone https://github.com/queeringthemap/queeringthemap.git && cd queeringthemap`
1. Make sure you have python 3 installed on your system.
    * On a Mac, if you have [Homebrew](https://brew.sh/) installed: `brew update` then `brew install python3`
1. Make sure you have the GDAL libraries installed on your system.
    * On a Mac, if you have [Homebrew](https://brew.sh/) installed: `brew install gdal`
1. Run `pipenv --python 3.5` to create a virtual environment with Python 3.5
1. Run `pipenv install --dev` to install the required dependencies
1. Is it working? Run `pipenv shell` and `./manage.py runserver` then open a browser and go to [http://127.0.0.1:8000/](http://127.0.0.1:8000/) to see!
1. To set up the local database, run `./manage.py migrate`
